package jp.alhinc.nagahama_morihiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String> map0 = new HashMap<String, String>();
		HashMap<String, Long> map1 = new HashMap<String, Long>();
		BufferedReader br = null;

		try {
			//コマンドライン引数で指定
			try {
				File file = new File(args[0], "branch.lst");
				//エラー処理
				if (!file.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}

				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while ((line = br.readLine()) != null) {
					String[] code = line.split(",");
					//エラー処理
					if (!code[0].matches("[0-9]{3}") || code.length != 2) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					map0.put(code[0], code[1]);
					map1.put(code[0], 0L);
				}
			} finally {
				if (br!= null)
					br.close();
			}

			//ファイルを検索、読み込み
			try {
				FilenameFilter filter = new FilenameFilter() {
					public boolean accept(File file, String str) {
						File fileA = new File(file, str);
						return (str.matches("^\\d{8}.rcd$") && fileA.isFile());
					}
				};

				File[] files = new File(args[0]).listFiles(filter);

				for (int i= 0; i < files.length; ++i) {
					//エラー処理
					if (i < files.length-1) {
						int number1 = Integer.parseInt(files[i].getName().split("\\.")[0]);
						int number2 = Integer.parseInt(files[i + 1].getName().split("\\.")[0]);
						if (number2 - number1 != 1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}

					String file1 = new String(files[i].getName());
					File file2 = new File(args[0],file1);
					br = new BufferedReader(new FileReader(file2));

					String line1 = br.readLine();
					//エラー処理
					if (!map1.containsKey(line1)) {
						System.out.println(file1 + "の支店コードが不正です");
						return;
					}
					//エラー処理
					try {
						Long sele1 = Long.valueOf(br.readLine());
						Long seleTotal = map1.get(line1);
						map1.put(line1,sele1 + seleTotal);

						//エラー処理
						Long amount = sele1 + seleTotal;
						int digit = amount.toString().length();
						if (digit > 10) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
					} catch (NumberFormatException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}

					//エラー処理
					if (br.readLine() != null) {
						System.out.println(file1 + "フォーマットが不正です");
						return;
					}
				}
			} finally {
				if (br!= null)
					br.close();
			}

			//全支店の支店コード、支店名、合計金額を出力する
			File lastFile = new File(args[0], "branch.out");
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(lastFile, false));

				for (Map.Entry<String, String> mo : map0.entrySet()) {
					String code1 = mo.getKey();
					String name1 = mo.getValue();
					Long output1 = map1.get(code1);
					bw.write(code1 + "," + name1 + "," + output1);
					bw.newLine();
				}
			} finally {
				if (bw!= null)
					bw.close();
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}

//課題の集計2の部分は、Map1を型変換後に28行目のvalue部分を変更
//64行目のMap getでkeyをLongへ代入
//63行目も型変更を行う。65行目のvalue部分にline2+totalを入れる
//繰り替え処理の中で行うことがポイント
//57～60行までは名前の検索行う処理
//61行目からはファイルを読み込む処理
//79行目からはファイルへの書き込みを行う
//81行目は拡張for文 , keyとvalueのセット
//82,84行目の右辺は同じ中身
//エラー処理1は" "の中を名前変えて、そこからif文で検索する。
